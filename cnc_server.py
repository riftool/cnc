import json
import os

from flask import Flask, request, jsonify, make_response
from queue_manager.queue_producer import QueueProducer

from commands_manager import CommandsManager

app = Flask(__name__)
AMQP_URL = os.environ.get('AMQP_URL')
DB_CONNECTION_STRING = os.environ.get('DB_CONNECTION_STRING')


@app.route('/send_command', methods=['POST'])
def send_command():
    cm = CommandsManager(connection_string=DB_CONNECTION_STRING)
    cm.add_command(**json.loads(request.json))
    return jsonify(success=True)


@app.route('/fetch_command', methods=['POST'])
def fetch_command():
    cm = CommandsManager(connection_string=DB_CONNECTION_STRING)
    res = cm.fetch_command(**json.loads(request.json))
    return jsonify(res)


@app.route('/leak', methods=['POST'])
def leak():
    data = json.loads(request.json)
    product_type = data.get('product_type')
    if not product_type:
        return make_response(jsonify(), 400)
    with QueueProducer(amqp_url=AMQP_URL, exchange_name=product_type) as producer:
        producer.publish(message=json.dumps(data))
    return jsonify(success=True)


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8000)
