import datetime
import json
from uuid import uuid4

from sqlalchemy import Column, String, JSON, INT, DateTime, Boolean, create_engine
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.exc import OperationalError
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm.exc import NoResultFound

from logger import logger

base = declarative_base()


class Commands(base):
    __tablename__ = 'commands'
    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid4, unique=True, nullable=False)
    tool_id = Column(String, nullable=False)
    params = Column(JSON)
    command = Column(String, nullable=False)
    priority = Column(INT, nullable=False)
    utc_insertion_time = Column(DateTime, default=datetime.datetime.utcnow)
    pooled_by_tool = Column(Boolean, default=False)

    def to_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


class CommandsManager:

    def __init__(self, connection_string):
        self.logger = logger
        self.engine = create_engine(connection_string)
        base.metadata.create_all(self.engine)

    def add_command(self, tool_id, command, params, priority):
        session = sessionmaker(self.engine)()
        try:
            command = Commands(tool_id=tool_id, command=command, params=params, priority=priority)
            session.add(command)
            session.commit()
        except OperationalError as e:
            logger.critical(f'Something went wrong {e}.')
        finally:
            session.bind.dispose()

    def fetch_command(self, tool_id):
        session = sessionmaker(self.engine)()
        try:
            command = session.query(Commands.id, Commands.command, Commands.params) \
                .filter(Commands.tool_id == tool_id) \
                .filter(Commands.pooled_by_tool.is_(False)) \
                .order_by(Commands.priority.desc()) \
                .first()
            if command:
                session.query(Commands) \
                    .filter(Commands.id == command.id) \
                    .update({Commands.pooled_by_tool: True})
                session.commit()
        except NoResultFound:
            command = {}
        finally:
            session.bind.dispose()
        return json.dumps({'id': str(command[0]), 'command': command[1], 'params': command[2]})
